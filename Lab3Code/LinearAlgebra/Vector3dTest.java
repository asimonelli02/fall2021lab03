package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Antonio Simonelli 1736100
 */

 public class Vector3dTest {

    @Test
    public void VectorTest(){
        Vector3d vector = new Vector3d(2, 6, 7);
        assertEquals(2, vector.getX());
        assertEquals(6, vector.getY());
        assertEquals(7, vector.getZ());
    }

    @Test
    public void magnitudeTest(){
        Vector3d vector = new Vector3d(3, 4, 0);
        assertEquals(5, vector.magnitude());
    }

    @Test
    public void dotProductTest(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        assertEquals(13, vector.dotProduct(vector2));
    }

    @Test
    public void addTest(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        Vector3d vectorResult = new Vector3d(3, 4, 6);
        assertEquals(vectorResult.getX(), vector.add(vector2).getX());
        assertEquals(vectorResult.getY(), vector.add(vector2).getY());
        assertEquals(vectorResult.getZ(), vector.add(vector2).getZ());
    }
}
